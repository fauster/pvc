import numpy as np
print("MDF 1D start")

# Entrada de dados
N = 6
h = 0.05
Ti = 200.0
dx = 2.0
connect = np.zeros((N-2,3), dtype=np.int32)
for i in range(N-2):
    connect[i] = [i, i+1, i+2]
bc = np.array([[1,300], [1,400]])

print(connect)
print(bc)

# MDF
A = np.zeros((N,N), dtype=np.float64)
b = np.zeros((N,1), dtype=np.float64)
for i in range(1,N-1):
    A[i,i-1] = -1
    A[i,i] = 2+h*(dx**2)
    A[i,i+1] = -1
    b[i] = h*(dx**2)*Ti

# Condição de contorno
if bc[0,0]==1:
    A[0,0] = 1
    b[0] = bc[0,1]
if bc[1,0]==1:
    A[N-1,N-1] = 1
    b[N-1] = bc[1,1]

print(A)
print(b)

x = np.linalg.solve(A, b)
print(x)
